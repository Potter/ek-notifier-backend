<?php

namespace AppBundle\Command;

use AppBundle\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

class FindPostCommand extends Command
{
    // This is just a normal Command::configure() method
    protected function configure()
    {
        $this->setName('ek:find:post')
             ->setDescription('Find posts in eK blog');
    }

    // Execute will be called in a endless loop
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        /** @var Registry $doctrine */
        $doctrine = $container->get('doctrine');
        $blogEntries = $doctrine->getRepository('AppBundle:Post')->findBy(['type' => 'blog'], ['id' => 'DESC']);
//        print_r(json_encode($blogEntries));

        $domain = 'http://www.ekreative.com';

        $siteBlog = file_get_contents($domain . '/blog');

        $DOM = new DOMDocument;

        libxml_use_internal_errors(true);
        $DOM->loadHTML($siteBlog);
        libxml_use_internal_errors(false);

        $xpath = new DOMXpath($DOM);

        $parsedPosts = $xpath->query('//div[@class="catItemBody"]');

        $postsToParse = 10;

        foreach ($parsedPosts as $parsedPost) {
            $postsToParse--;
            if ($postsToParse <= 0) {
                break;
            }

            $title = $xpath->query('div[@class="catItemHeader"]/h2/a', $parsedPost);
            $link = $title->item(0)->attributes->getNamedItem('href')->value;
            if (strpos($link, 'http') === FALSE) {
                $link = $domain . $link;
            }
            $title = trim($title->item(0)->nodeValue);

            $author = $xpath->query('span[@class="catItemAuthor"]/a', $parsedPost)->item(0)->nodeValue;
            $text = $xpath->query('div[@class="desc_border"]/div/p', $parsedPost)->item(0)->nodeValue;
            $date = $xpath->query('div[@class="read_more_wrap"]/div[@class="blogLeftBlock"]/span', $parsedPost)->item(0)->nodeValue;
            $date = new \DateTime($date);

            $image = $xpath->query('div/span[@class="catItemImage"]/a/img', $parsedPost);
            $image = $image->item(0)->attributes->getNamedItem('src')->value;
            if (strpos($image, 'http') === FALSE) {
                $image = $domain . $image;
            }

            $doAddNewPost = true;
            foreach ($blogEntries as $blogEntry) {
                if ($blogEntry->getLink() == $link) {
                    $doAddNewPost = false;
                }
            }
            if ($doAddNewPost) {
                $post = new Post();
                $post->setAuthor($author);
                $post->setDatetime($date);
                $post->setImage($image);
                $post->setLink($link);
                $post->setText($text);
                $post->setTitle($title);
                $post->setType('blog');
                $doctrine->getManager()->persist($post);
                $doctrine->getManager()->flush();
                echo "Added post\n";
            }
        }
    }
}
