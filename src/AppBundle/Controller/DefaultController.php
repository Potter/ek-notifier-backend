<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/data", name="datapage")
     */
    public function dataAction(Request $request)
    {
        $items = $this->getDoctrine()->getRepository('AppBundle:Post')->findBy([], ['id' => 'DESC'], 10);
        return new JsonResponse(['items' => $items], 200);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello! This is backend for ek-notifier. Go to <a href="/admin">/admin</a>');
    }
}
