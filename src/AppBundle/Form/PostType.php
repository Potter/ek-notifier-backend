<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', ['attr' => ['class' => 'form-control']])
            ->add('type', 'choice', [ 'choices' => ['notify' => 'Notify', 'office' => 'Office alert', 'blog' => 'Blog'], 'required' => true, 'attr' => ['class' => 'form-control']])
            ->add('text', 'text', ['attr' => ['class' => 'form-control']])
            ->add('datetime', 'datetime', ['data' => new \DateTime(), 'attr' => ['class' => 'datetime-editor']])
            ->add('image', 'text', ['required' => false, 'attr' => ['class' => 'form-control']])
            ->add('link', 'text', ['required' => false, 'attr' => ['class' => 'form-control']])
            ->add('author', 'text', ['attr' => ['class' => 'form-control']])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_post';
    }
}
